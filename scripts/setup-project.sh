#!/usr/bin/env bash

# Raise an error if not used as intended and display the correct usage
if [[ $# != 1 ]]; then
	echo "Usage: ${0} [EP-NUM]"
	exit 1
fi

EP_NUM=$1

# Define the running directory
cd "$(dirname ${BASH_SOURCE[0]})"
RUN_DIR="$(pwd)/EP-$EP_NUM"

# Create the running directory (EP-$EP_NUM) with subfolders
mkdir -p ${RUN_DIR}/Source/{"Graphics","Kdenlive Project","Text Material","Video Material"}